# CPM download version, check last version: https://github.com/cpm-cmake/CPM.cmake/releases/latest/download/get_cpm.cmake
set(CPM_VERSION 0.35.0)

# CMake templates direction
set(CMAKE_DIR ".")

# CPM download direction
set(CPM_DIR ${CMAKE_DIR}/CPM.cmake)

# Check if CPM file exist, if not fetch it
if(NOT (EXISTS ${CPM_DIR}))
    message(STATUS "Downloading CPM.cmake")
    file(DOWNLOAD https://github.com/TheLartians/CPM.cmake/releases/download/v${CPM_VERSION}/CPM.cmake ${CPM_DIR})
else()
    message(STATUS "CPM.cmake found")
endif()

# Include the CPM file to be able to use
if(EXISTS ${CPM_DIR})
    include(${CMAKE_BINARY_DIR}/CPM.cmake)
else()
    message(SEND_ERROR "It seems CPM file could not fetch properly, run cmake again!")
endif()

#function for fetching & building dependency
function(CheckDependency name repository version)
    CPMAddPackage(
        NAME ${name}                          # The unique name of the dependency (should be the exported target's name)
        GITHUB_REPOSITORY ${repository}
        VERSION ${version}                    # The minimum version of the dependency (optional, defaults to 0)
        #OPTIONS "CXXOPTS_ENABLE_INSTALL YES" # e.g. "CXXOPTS_BUILD_EXAMPLES NO" "CXXOPTS_BUILD_TESTS NO" 
        #GIT_TAG e.g. "boost-1.77.0"
        #DOWNLOAD_ONLY YES/NO                 # If set, the project is downloaded, but not configured (optional)
        )
endfunction()
