#creates a target that runs doxygen
function(ADD_DOCUMENTATION_TARGET)
    cmake_parse_arguments(
        PARSE_ARGV 0
        DOCUMENTATION
        ""
        "DOC_DIR"
        "SRC_DIRS"
    )

    find_package(Doxygen REQUIRED dot)

    set(doxyfile ${DOCUMENTATION_DOC_DIR}/Doxyfile)
    set(DOXYGEN_INPUT ${DOCUMENTATION_SRC_DIRS})
    set(DOXYGEN_OUTPUT_DIRECTORY ${DOCUMENTATION_DOC_DIR}/documentation-generated)

    add_custom_target(generate_documentation
    COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
    WORKING_DIRECTORY ${DOCUMENTATION_DOC_DIR}
    COMMENT "Generating Documentation with Doxygen"
    VERBATIM)
endfunction()

#creates a target that makes translations
function(ADD_TRANSLATION_TARGET)
    cmake_parse_arguments(
        PARSE_ARGV 0
        TRANSLATION
        ""
        "TS_DIR;PARENT_TARGET"
        ""
    )
    ## find linguist tool for translations
    find_package(Qt5 COMPONENTS LinguistTools REQUIRED)
    message("Adding translation targets...(TS DIR: ${TRANSLATION_TS_DIR})")
    file(GLOB TRANSLATIONS_TS_FILES CONFIGURE_DEPENDS "${TRANSLATION_TS_DIR}/*.ts")
    message("translations detected: ${TRANSLATIONS_TS_FILES}")
    add_custom_target(create_translations
      COMMAND ${Qt5_LUPDATE_EXECUTABLE} ${CMAKE_SOURCE_DIR} -ts ${TRANSLATIONS_TS_FILES} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
    add_custom_target(release_translations
      COMMAND ${Qt5_LRELEASE_EXECUTABLE} ${TRANSLATIONS_TS_FILES}
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      DEPENDS create_translations)
    add_dependencies(${TRANSLATION_PARENT_TARGET} release_translations)
    message("Translations will build when ${TRANSLATION_PARENT_TARGET} will build")
    message("Done!")
endfunction()

# Detect OS and run the related deployment function
# Only deploys on release mode!
function(deployDesktopApp target QtBinDir QmlDir)
    if (WIN32 OR MSVC)
        windeployqt(${target} ${QtBinDir} ${QmlDir})
    elseif(UNIX AND NOT APPLE)
        # Look if CQtDeployer is installed on system
        find_program(CQT_DEPLOYER "cqtdeployer")
        # If not call install function
        if(NOT CQT_DEPLOYER)
            InstallCQtDeployer()
        endif()
        linuxdeployqt(${target} ${QmlDir})
    endif ()
endfunction()

# Function for deploying on window
function(windeployqt target QtBinDir QmlDir)
    # POST_BUILD step
    # - we run windeployqt on target and deploy Qt libs
    if(${CMAKE_BUILD_TYPE} STREQUAL Release)
        add_custom_command(TARGET ${target} POST_BUILD
            COMMAND "${QtBinDir}/windeployqt.exe"
            --qmldir ${CMAKE_SOURCE_DIR}/${QmlDir}
            --compiler-runtime
            \"$<TARGET_FILE:${target}>\"
            COMMENT "Deploying Qt libraries using windeployqt for compilation target '${target}'"
        )
    endif()
endfunction()

# Function for deploying on linux
function(linuxdeployqt target QmlDir)
    if(${CMAKE_BUILD_TYPE} STREQUAL Release)
        add_custom_command(TARGET ${target} POST_BUILD
            COMMAND "cqtdeployer"
            -qmlDir ${CMAKE_SOURCE_DIR}/${QmlDir}
            -bin ${PROJECT_NAME}
            COMMENT "Deploying Qt libraries using cqtdeployer for compilation target '${target}'"
        )
    endif()
endfunction()

# Function for fetching & installing CQtDeployer
# First check if installer file exist, if not download the installer & install the program
function(InstallCQtDeployer)
    set(CQT_VERSION "1.5.4.14")
    set(INSTALLER_NAME "CQtDeployer_${CQT_VERSION}_OfflineInstaller_Linux_x86_64.run")
    set(INSTALLER_DIR "/tmp/CQtDeployerInstaller.run")
    if(NOT EXISTS ${INSTALLER_DIR})
        message(STATUS "CQtDeployer could not found, downloading..")
        file(DOWNLOAD https://github.com/QuasarApp/CQtDeployer/releases/download/v${CQT_VERSION}/${INSTALLER_NAME}
            ${INSTALLER_DIR}
        )
        message(STATUS "CQtDeployer installing..")
    endif()
    execute_process(COMMAND "chmod" "+x" ${INSTALLER_DIR})
    execute_process(COMMAND ${INSTALLER_DIR} "-c" "install"
        WORKING_DIRECTORY $ENV{HOME}
    )
endfunction()

# Function for adding packages
function(addPackage)
    list(APPEND REQUIRED_QT_PACKAGES Core)
    if(${USE_QML})
        list(APPEND REQUIRED_QT_PACKAGES Quick)
        list(APPEND QML_IMPORT_PATH "${CMAKE_SOURCE_DIR}/${QML_DIR}")
        set(QML_IMPORT_PATH ${QML_IMPORT_PATH}
            CACHE STRING "Qt Creator Import Path"
            FORCE)
    endif()
    if(${USE_WIDGETS})
        list(APPEND REQUIRED_QT_PACKAGES Widgets Gui)
    endif()
    if(${USE_TESTS})
        list(APPEND REQUIRED_QT_PACKAGES Test)
    endif()
    if(${USE_TRANSLATIONS})
        list(APPEND NOLIB_QT_PACKAGES LinguistTools)
    endif()
    foreach(QT_PACKAGE ${REQUIRED_QT_PACKAGES})
        list(APPEND REQUIRED_QT_LIBS Qt${QT_VERSION_MAJOR}::${QT_PACKAGE})
    endforeach()
    find_package(QT NAMES Qt${QT_VERSION_MAJOR} COMPONENTS ${REQUIRED_QT_PACKAGES} ${NOLIB_QT_PACKAGES} REQUIRED)
    find_package(Qt${QT_VERSION_MAJOR} COMPONENTS ${REQUIRED_QT_PACKAGES} ${NOLIB_QT_PACKAGES} REQUIRED)
    set(REQUIRED_QT_LIBS ${REQUIRED_QT_LIBS} PARENT_SCOPE)
    set(NOLIB_QT_PACKAGES ${NOLIB_QT_PACKAGES} PARENT_SCOPE)
endfunction()

# Function for linking sources to executable for target platforms
function(addPlatform)
    # ---SETTING ICONS---
    if(WINDOWS_TARGET)
        # Setting WINDOWS Icon
        set(APP_ICON_RESOURCE "${CMAKE_CURRENT_SOURCE_DIR}/targets/windows/icon/winIcon.rc")
        set(APP_ICON "${CMAKE_CURRENT_SOURCE_DIR}/targets/windows/icon/winIcon.ico")
    endif()
    if(MACOS_TARGET)
        # Setting MACOS Icon
        set(MACOSX_BUNDLE_ICON_FILE macIcon.icns)
        set(APP_ICON ${CMAKE_CURRENT_SOURCE_DIR}/targets/macos/icon/macIcon.icns)
        set_source_files_properties(${APP_ICON} PROPERTIES
            MACOSX_PACKAGE_LOCATION "Resources")

        # To build MACOS bundle
        add_executable(${PROJECT_NAME}
          MACOSX_BUNDLE
            ${SOURCES}
            ${APP_ICON}
            )
    endif()
    if(LINUX_TARGET)
      # Copying icon file to the build directory to make it visible in the left upper corner
      file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/targets/windows/icon/winIcon.ico
           DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/targets/windows/icon/)

        add_executable(${PROJECT_NAME}
            ${SOURCES}
            ${APP_ICON_RESOURCE}
            ${APP_ICON}
            )
    endif()
    #########################################
    # TO DO: Implement for android platform
    #
    #if(ANDROID_TARGET)
    #
    #endif()
    #########################################
    # TO DO: Implement for iOS platform
    #
    #if(IOS_TARGET)
    #
    #endif()
    #########################################
endfunction()
